// khai báo thư viện mongoose
const mongoose = require("mongoose");
// khai báo class schema của mongo
const schema = mongoose.Schema;
// Khai báo user schema 
const geoSchema = new schema({
    lat: {type: String,required: true},
    lng: {type: String,required: true}
},{_id:false});
const addressSchema = new schema ({
    street:{type: String,required: true},
    suite: {type: String,required: true},
    city: {type: String,required: true},
    zipcode: {type: String,required: true},
    geo: geoSchema
},{_id:false});
 const companySchema = new schema ({
    name: {type: String,required: true},
    catchPhrase: {type: String,required: true},
    bs: {type: String,required: true}
 },{_id:false});
 const userSchema = new schema ({
    _id: {type: mongoose.Types.ObjectId, require: true},
    name: {type: String,required: true},
    username: {type: String,required: true,unique: true},
    email:{type: String, require: true},
    address: addressSchema,
    phone: {type: String,required: true},
    website: {type: String,require: true},
    company: companySchema
 },{timestamps:true});

 // exprot mongoose
 module.exports = mongoose.model("user",userSchema);