// khai báo thư viện mongoose
const mongoose = require("mongoose");
// khai báo class schema của mongo
const schema = mongoose.Schema;
//khai báo  review schema
const photoSchema = new schema({
    albumId:{type:mongoose.Types.ObjectId, ref:"album"},
    title:{type:String, require: true},
    url:{type:String, require: true},
    thumbnailUrl:{type:String, require: true}
},{timestamps:true});
// exprot model
module.exports = mongoose.model("photo",photoSchema);