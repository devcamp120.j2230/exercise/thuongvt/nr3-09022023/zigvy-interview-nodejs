// khai báo thư viện mongoose
const mongoose = require("mongoose");
// khai báo class schema của mongo
const schema = mongoose.Schema;
//khai báo  review schema
const todoSchema = new schema ({
    userId:{type:mongoose.Types.ObjectId, ref:"user"},
    title:{type:String, require: true},
    completed:{type:Boolean}
},{timestamps:true})

// exprot mongoose
module.exports = mongoose.model("todo",todoSchema);
