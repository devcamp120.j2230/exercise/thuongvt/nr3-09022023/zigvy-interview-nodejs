// khai báo thư viện mongoose
const mongoose = require("mongoose");
// khai báo class schema của mongo
const schema = mongoose.Schema;
// Khai báo comment schema 
const commentSchema = new schema({
    _id:{type: mongoose.Types.ObjectId, require: true},
    postId:{type: mongoose.Types.ObjectId, ref:"post"},
    name:{type: String, require:true},
    email:{type: String, require:true},
    body:{type: String, require:true}
},{timestamps:true});

//exprot modul
module.exports = mongoose.model("comment",commentSchema)