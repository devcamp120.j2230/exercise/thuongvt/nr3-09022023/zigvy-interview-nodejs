// khai báo thư viện mongoose
const mongoose = require("mongoose");
// khai báo class schema của mongo
const schema = mongoose.Schema;
//khai báo  review schema
const albumSchema = new schema({
    userId:{type:mongoose.Types.ObjectId, ref:"user"},
    title:{type:String, require: true}
},{timestamps:true})
//exprot model
module.exports = mongoose.model("album",albumSchema);