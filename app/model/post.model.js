// khai báo thư viện mongoose
const mongoose = require("mongoose");
// khai báo class schema của mongo
const schema = mongoose.Schema;
// Khai báo post schema 
const postSchema =  new schema ({
    _id:{type: mongoose.Types.ObjectId, require: true},
    userId:{type: mongoose.Types.ObjectId, ref:"user"},
    title:{type: String, require: true},
    body:{type:String,require:true}
});
module.exports = mongoose.model("post",postSchema);