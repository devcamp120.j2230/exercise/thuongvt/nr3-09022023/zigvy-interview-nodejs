//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const middlerware = require("../middleware/middleware");
//Khai báo controller
const albumController = require("../controller/album.controller");
//Tạo router
const albumRouter = express.Router();

albumRouter.post("/albums",middlerware.Middeware,albumController.createAlbum);
albumRouter.get("/albums",middlerware.Middeware,albumController.getAllAlbum);
albumRouter.put("/albums/:albumId",middlerware.Middeware,albumController.updateAlbumById);
albumRouter.get("/albums/:albumId",middlerware.Middeware,albumController.getAlbumById);
albumRouter.delete("/albums/:albumId",middlerware.Middeware,albumController.deleteAlbumById);
albumRouter.get("/users/:userId/albums",middlerware.Middeware,albumController.getAlbumsOfUser);
//exprot modul
module.exports ={albumRouter};
