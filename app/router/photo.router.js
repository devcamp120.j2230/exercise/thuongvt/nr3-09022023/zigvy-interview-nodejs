//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const middlerware = require("../middleware/middleware");
//Khai báo controller
const photoController = require("../controller/photo.controller");
//Khai báo photoRouter 
const photoRouter = express.Router();

photoRouter.post("/photos",middlerware.Middeware,photoController.createPhoto);
photoRouter.get("/photos",middlerware.Middeware,photoController.getAllPhoto);
photoRouter.put("/photos/:photoId",middlerware.Middeware,photoController.updatePhotoById);
photoRouter.get("/photos/:photoId",middlerware.Middeware,photoController.getPhotoById);
photoRouter.delete("/photos/:photoId",middlerware.Middeware,photoController.deletePhotoById);
photoRouter.get("/albums/:albumId/photos",middlerware.Middeware,photoController.getPhotosOfAlbum);


module.exports = {photoRouter}