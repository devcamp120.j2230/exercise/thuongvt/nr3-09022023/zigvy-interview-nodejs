//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const middlerware = require("../middleware/middleware");
//tạo Router
const todoRouter = express.Router();
// improt controller
const todoController= require("../controller/todo.controller");

todoRouter.post("/todos",middlerware.Middeware,todoController.createTodo)
todoRouter.get("/todos",middlerware.Middeware,todoController.getAllTodo)
todoRouter.put("/todos/:todoId",middlerware.Middeware,todoController.updateTodoById);
todoRouter.delete("/todos/:todoId",middlerware.Middeware,todoController.deleteTodoById)
todoRouter.get("/todos/:todoId",middlerware.Middeware,todoController.getTodoById)
todoRouter.get("/users/:userId/todos",middlerware.Middeware,todoController.getTodosOfUser)

// exprot router
module.exports = {todoRouter};
