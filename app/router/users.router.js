//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const middlerware = require("../middleware/middleware");
//Khai báo controller
const userController = require("../controller/users.controller");
//tạo user router 
const userRouter = express.Router();

//tạo các API
userRouter.post("/users",middlerware.Middeware,userController.createUser);
userRouter.get("/users",middlerware.Middeware,userController.getAllUser);
userRouter.put("/users/:userId",middlerware.Middeware,userController.updateUserById);
userRouter.get("/users/:userId",middlerware.Middeware,userController.getUserById);
userRouter.delete("/users/:userId",middlerware.Middeware,userController.deleteUserById);

// exprot thành modul
module.exports = {userRouter}