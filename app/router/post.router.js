//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const middlerware = require("../middleware/middleware");
//Khai báo controller
const postController = require("../controller/post.controller");
//Tạo ruoter
const postRouter = express.Router();

postRouter.post("/posts",middlerware.Middeware,postController.createPost);
postRouter.get("/posts",middlerware.Middeware,postController.getAllPost);
postRouter.put("/posts/:postId",middlerware.Middeware,postController.updatePostById);
postRouter.get("/posts/:postId",middlerware.Middeware,postController.getPostById);
postRouter.delete("/posts/:postId",middlerware.Middeware,postController.deletePostById);
postRouter.get("/users/:userId/posts",middlerware.Middeware,postController.getPostsOfUse);



//exprot thành modul
module.exports = {postRouter};