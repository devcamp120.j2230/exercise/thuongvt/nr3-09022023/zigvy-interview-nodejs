//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const middlerware = require("../middleware/middleware");
//Khai báo controller
const commentController = require("../controller/comment.controller");
//Tạo Router
const commentRouter = express.Router();

commentRouter.post("/comments",middlerware.Middeware,commentController.createComment);
commentRouter.get("/comments",middlerware.Middeware,commentController.getAllComment);
commentRouter.put("/comments/:commentId",middlerware.Middeware,commentController.updateCommentById)
commentRouter.get("/comments/:commentId",middlerware.Middeware,commentController.getCommentById)
commentRouter.delete("/comments/:commentId",middlerware.Middeware,commentController.deleteCommentById);
commentRouter.get("/posts/:postId/comments",middlerware.Middeware,commentController.getCommentsOfPost)

//exprot modul
module.exports = {commentRouter}