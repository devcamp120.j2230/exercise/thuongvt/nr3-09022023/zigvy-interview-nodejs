// import thư viện mongose 
const mongoose = require("mongoose");
//improt Model 
const photoModel = require("../model/photo.model")
//Xây dựng API createPhoto:
const createPhoto =(req,res)=>{
    //B1 thu thập dữ liệu từ req
    let body = req.body;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.albumId)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    if(!body.title){
        return res.status(400).json({
            message: 'dữ liệu title không đúng'
          })
    }
    if(!body.url){
        return res.status(400).json({
            message: 'dữ liệu url không đúng'
          })
    }
    if(!body.thumbnailUrl){
        return res.status(400).json({
            message: 'dữ liệu thumbnailUrl không đúng'
          })
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    // chuẩn bị dữ liệu
    let newPhoto ={
        _id: mongoose.Types.ObjectId(),
        albumId: body.albumId,
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl
    }
    // tạo dữ liệu mới
    photoModel.create(newPhoto,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"Tạo mới thành công",
                photo: data
            })
        }
    })
}
//Xây dựng API getAllPhoto
const getAllPhoto = (req,res)=>{
    //B1 thu thập dữ liệu từ req
    let {albumId} = req.query;
    let conditionCheck = {};
    //B2 Kiểm tra dữ liệu
    if(albumId){
        conditionCheck.albumId = albumId
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    photoModel.find(conditionCheck)
        .populate({
            path:"albumId",
            populate:{
                path:"userId",
                model:"user"
            }
        })
        .exec((err,data)=>{
            if(err){
                return res.status(500).json({
                    message: err
                })
            }
            else{
                return res.status(201).json({
                    message:"Tải thành công toàn bộ photo",
                    photo: data
                })
            }
        })
}
// Xây dựng API updatePhotoById:
const updatePhotoById = (req,res)=>{
    //B1 thu thập dữ liệu từ req
    let id = req.params.photoId;
    let body = req.body;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    if(!body.title){
        return res.status(400).json({
            message: 'dữ liệu title không đúng'
          })
    }
    if(!body.url){
        return res.status(400).json({
            message: 'dữ liệu url không đúng'
          })
    }
    if(!body.thumbnailUrl){
        return res.status(400).json({
            message: 'dữ liệu thumbnailUrl không đúng'
          })
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    let updatePhoto ={
        albumId: body.albumId,
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl
    }
    photoModel.findByIdAndUpdate(id,updatePhoto,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"cập nhật thành công dữ liệu",
                Photo: data
            })
        }
    })
}
// Xây dựng API getPhotoById
const getPhotoById = (req,res)=>{
    let id = req.params.photoId;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    photoModel.findById(id).populate({
        path:"albumId",
        populate:{
            path:"userId",
            model:"user"
        }
    })
    .exec((err,data)=>{
        if(err){
            return res.status(500).json({
                message: err
            })
        }
        else{
            return res.status(201).json({
                message:"Tải thành công photo by Id",
                photo: data
            })
        }
    })
}
// API deletePhotoById
const deletePhotoById = (req,res)=>{
    //B1 thu thập dữ liệu từ req
    let id = req.params.photoId;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    photoModel.findByIdAndDelete(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"xoá thành công dữ liệu",
                Photo: data
            })
        }
    })
}
// Xây dựng API getPhotosOfAlbum:
const getPhotosOfAlbum = (req,res)=>{
    //B1 thu thập dữ liệu từ req
    let albumId = req.params.albumId;
    let condition ={}
    //B2 Kiểm tra dữ liệu
    if(albumId){
        condition.albumId = albumId
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    photoModel.find(condition)
        .populate({
            path:"albumId",
            populate:{
                path:"userId",
                model:"user"
            }
        })
        .exec((err,data)=>{
            if(err){
                return res.status(500).json({
                    message: err.message
                })
            }
            else{
                return res.status(201).json({
                    message:"lấy thành công dữ liệu getPhotosOfAlbum:",
                    Photo: data
                })
            }
        })
    }
//exprot controller
module.exports = {
    createPhoto,
    getAllPhoto,
    updatePhotoById,
    deletePhotoById,
    getPhotosOfAlbum,
    getPhotoById
}

