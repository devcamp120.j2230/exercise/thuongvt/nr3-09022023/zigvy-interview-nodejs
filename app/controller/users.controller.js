// import thư viện mongose 
const mongoose = require("mongoose");
//improt UserModel 
const userModel = require("../model/users.model");
//tạo một user mới
const createUser = (req, res) => {
    //B1: thu thập dữ liệu từ req
    let body = req.body
    //B2: validate dữ liệu
    if (!body.name) {
        return res.status(400).json({
            message: 'dữ liệu name chưa đúng'
        })
    }
    if (!body.username) {
        return res.status(400).json({
            message: 'dữ liệu username chưa đúng'
        })
    }
    if (!body.email) {
        return res.status(400).json({
            message: 'dữ liệu email chưa đúng'
        })
    }
    if (!body.address.street) {
        return res.status(400).json({
            message: 'dữ liệu street chưa đúng'
        })
    }
    if (!body.address.suite) {
        return res.status(400).json({
            message: 'dữ liệu suite chưa đúng'
        })
    }
    if (!body.address.city) {
        return res.status(400).json({
            message: 'dữ liệu city chưa đúng'
        })
    }
    if (!body.address.zipcode) {
        return res.status(400).json({
            message: 'dữ liệu zipcode chưa đúng'
        })
    }
    if (!body.address.geo.lat) {
        return res.status(400).json({
            message: 'dữ liệu lat chưa đúng'
        })
    }
    if (!body.address.geo.lng) {
        return res.status(400).json({
            message: 'dữ liệu lat chưa đúng'
        })
    }
    if(!body.phone){
        return res.status(400).json({
            message: 'dữ liệu phone chưa đúng'
        })
    }
    if(!body.website){
        return res.status(400).json({
            message: 'dữ liệu website chưa đúng'
        })
    }
    if(!body.company.name){
        return res.status(400).json({
            message: 'dữ liệu company name chưa đúng'
        })
    }
    if(!body.company.catchPhrase){
        return res.status(400).json({
            message: 'dữ liệu company catchPhrase chưa đúng'
        })
    }
    if(!body.company.bs){
        return res.status(400).json({
            message: 'dữ liệu company bs chưa đúng'
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newUsserData = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        username: body.username,
        email : body.email,
        address: {
            street : body.address.street,
            suite: body.address.suite,
            city: body.address.city,
            zipcode: body.address.zipcode,
            geo: {
                lat: body.address.geo.lat,
                lng: body.address.geo.lng
            }
        },
        phone: body.phone,
        website: body.website,
        company: {
            name: body.company.name,
            catchPhrase: body.company.catchPhrase,
            bs: body.company.bs
        }
    }
    userModel.create(newUsserData,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"Tạo mới thành công",
                User: data
            })
        }
    }) 
}
// Xây dựng API getAllUser
const getAllUser = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải toàn bộ dữ liệu thành công",
                 User: data
             })
         }
    })
}
// Xây dựng API updateUserById
const updateUserById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.userId;
    let body = req.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(400).json({
            message: " Id không đúng"
        })
    }
    if (!body.name) {
        return res.status(400).json({
            message: 'dữ liệu name chưa đúng'
        })
    }
    if (!body.username) {
        return res.status(400).json({
            message: 'dữ liệu username chưa đúng'
        })
    }
    if (!body.email) {
        return res.status(400).json({
            message: 'dữ liệu email chưa đúng'
        })
    }
    if (!body.address.street) {
        return res.status(400).json({
            message: 'dữ liệu street chưa đúng'
        })
    }
    if (!body.address.suite) {
        return res.status(400).json({
            message: 'dữ liệu suite chưa đúng'
        })
    }
    if (!body.address.city) {
        return res.status(400).json({
            message: 'dữ liệu city chưa đúng'
        })
    }
    if (!body.address.zipcode) {
        return res.status(400).json({
            message: 'dữ liệu zipcode chưa đúng'
        })
    }
    if (!body.address.geo.lat) {
        return res.status(400).json({
            message: 'dữ liệu lat chưa đúng'
        })
    }
    if (!body.address.geo.lng) {
        return res.status(400).json({
            message: 'dữ liệu lat chưa đúng'
        })
    }
    if(!body.phone){
        return res.status(400).json({
            message: 'dữ liệu phone chưa đúng'
        })
    }
    if(!body.website){
        return res.status(400).json({
            message: 'dữ liệu website chưa đúng'
        })
    }
    if(!body.company.name){
        return res.status(400).json({
            message: 'dữ liệu company name chưa đúng'
        })
    }
    if(!body.company.catchPhrase){
        return res.status(400).json({
            message: 'dữ liệu company catchPhrase chưa đúng'
        })
    }
    if(!body.company.bs){
        return res.status(400).json({
            message: 'dữ liệu company bs chưa đúng'
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    const newDataUpdate = {
        name: body.name,
        username: body.username,
        email : body.email,
        address: {
            street : body.address.street,
            suite: body.address.suite,
            city: body.address.city,
            zipcode: body.address.zipcode,
            geo: {
                lat: body.address.geo.lat,
                lng: body.address.geo.lng
            }
        },
        phone: body.phone,
        website: body.website,
        company: {
            name: body.company.name,
            catchPhrase: body.company.catchPhrase,
            bs: body.company.bs
        }
    }
    userModel.findByIdAndUpdate(Id,newDataUpdate).exec((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"Cập nhật dữ liệu thành công bằng  ID",
                 updateData: data
             })
         }
    })
}
// Xây dựng API getUserById
const getUserById = (req,res)=>{
     //B1: thu thập dữ liệu từ req
     let Id = req.params.userId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(400).json({
            message: " Id không đúng"
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.findById(Id).exec((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tìm dữ liệu thành công thông qua ID",
                 User: data
             })
         }
    })
}
// Xây dựng API deleteUserById
const deleteUserById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.userId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(400).json({
            message: " Id không đúng"
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.findByIdAndDelete(Id).exec((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"xóa dữ liệu thành công thông qua ID",
                 User: data
             })
         }
    })
}
// export modul
module.exports = {
    createUser,
    getAllUser,
    updateUserById,
    getUserById,
    deleteUserById
}