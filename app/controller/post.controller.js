// khai báo thu viện mongose
const mongoose = require("mongoose");
// Khai báo postModel
const postModel = require("../model/post.model");
// Xây dựng API createPost
const createPost = (req, res) => {
    //B1 thu thập dữ liệu
    let body = req.body
    //B2 kiểm tra dữ liệu
    if (!body.title) {
        return res.status(400).json({
            message: "Dữ liệu title không đúng"
        })
    }
    if (!body.body) {
        return res.status(400).json({
            message: "Dữ liệu body không đúng"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.userId)) {
        return res.status(400).json({
            message: "Dữ liệu userId không đúng"
        })
    }
    //B3 gọi model thực hiện thao tác nghiệp vụ
    let newDataUpdate = {
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title,
        body: body.body
    }
    postModel.create(newDataUpdate, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "Tạo mới thành công",
                Post: data
            })
        }
    })
}
// Xây dựng API getAllPost:
const getAllPost = (req, res) => {
    //B1: thu thập dữ liệu từ req
    let {userId} = req.query // query là một cặp (key-vale) nên phải để trong một {}
    let condition = {};
    if(userId){
        condition.userId = userId;
    }
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    postModel.find(condition).exec((err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "Tải toàn bộ dữ liệu thành công",
                Post: data
            })
        }
    })
}
// Xây dựng API updatePostById
const updatePostById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.postId;
    let body = req.body
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: "Dữ liệu Id không đúng"
        })
    }
    if (!body.title) {
        return res.status(400).json({
            message: "Dữ liệu title không đúng"
        })
    }
    if (!body.body) {
        return res.status(400).json({
            message: "Dữ liệu body không đúng"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.userId)) {
        return res.status(400).json({
            message: "Dữ liệu userId không đúng"
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let postDataUpdate = {
        userId: body.userId,
        title: body.title,
        body: body.body
    }
    postModel.findByIdAndUpdate(Id, postDataUpdate).exec((err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(200).json({
                message: "Cập nhật dữ liệu thành công bằng  ID",
                updateData: data
            })
        }
    })
}
// Xây dựng API getPostById
const getPostById = (req, res) => {
    //B1: thu thập dữ liệu từ req
    let Id = req.params.postId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: "Dữ liệu Id không đúng"
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    postModel.findById(Id).populate("userId").exec((err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "Tìm dữ liệu thành công thông qua ID",
                Post: data
            })
        }
    })
}
// Xây dựng API deletePostById
const deletePostById = (req, res) => {
    //B1: thu thập dữ liệu từ req
    let Id = req.params.postId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: "Dữ liệu Id không đúng"
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    postModel.findByIdAndDelete(Id).exec((err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "xóa dữ liệu thành công thông qua ID",
                Post: data
            })
        }
    })
}
// Xây dựng API getPostsOfUse
const getPostsOfUse = (req, res) => {
    //B1: thu thập dữ liệu từ req
    let userId = req.params.userId
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "Dữ liệu Id không đúng"
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    postModel.find({userId:userId}).populate("userId").exec((err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "Tìm dữ liệu thành công thông qua ID",
                Post: data
            })
        }
    })
}

// exprot modul
module.exports = {
    createPost,
    getAllPost,
    updatePostById,
    getPostById,
    deletePostById,
    getPostsOfUse,

}