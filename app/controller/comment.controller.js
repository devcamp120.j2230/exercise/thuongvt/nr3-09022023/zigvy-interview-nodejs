// khai báo thu viện mongose
const mongoose = require("mongoose");
// Khai báo commentModel
const commentModel = require("../model/comment.model");
// Xây dựng API createComment
const createComment = (req,res)=>{
    //B1 thu thập dữ liệu
    let body = req.body;
    //Kiểm tra dữ liệu
    if(!body.name){
        return res.status(400).json({
            message: "Dữ liệu name không đúng"
        })
    }
    if(!body.email){
        return res.status(400).json({
            message: "Dữ liệu email không đúng"
        })
    }
    if(!body.body){
        return res.status(400).json({
            message: "Dữ liệu body không đúng"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.postId)){
        return res.status(400).json({
            message: "Dữ liệu postId không đúng"
        })
    }
    //B3 Thực hiện tạo mới dữ liệu
    let newDataComment = {
        _id: mongoose.Types.ObjectId(),
        postId:body.postId,
        name: body.name,
        email: body.email,
        body: body.body
    }
    commentModel.create(newDataComment,(err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "Tạo mới thành công",
                Post: data
            })
        }
    })
}
// Xây dựng API getAllComment
const getAllComment = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let {postId} = req.query // query là một cặp (key-vale) nên phải để trong một {}
    let condition = {};
    if(postId){
        condition.postId = postId;
    }
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    commentModel.find(condition).populate("postId").exec((err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "Tải toàn bộ dữ liệu thành công",
                Comment: data
            })
        }
    })
}

// Xây dựng API updateCommentById:
const updateCommentById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.commentId;
    let body = req.body
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(400).json({
            message: "Dữ liệu Id không đúng"
        })
    }
    if(!body.name){
        return res.status(400).json({
            message: "Dữ liệu name không đúng"
        })
    }
    if(!body.email){
        return res.status(400).json({
            message: "Dữ liệu email không đúng"
        })
    }
    if(!body.body){
        return res.status(400).json({
            message: "Dữ liệu body không đúng"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.postId)){
        return res.status(400).json({
            message: "Dữ liệu postId không đúng"
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let CommentDataUpdate = {
        postId:body.postId,
        name: body.name,
        email: body.email,
        body: body.body
    }
    commentModel.findByIdAndUpdate(Id,CommentDataUpdate).exec((err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(200).json({
                message: "Cập nhật dữ liệu thành công bằng  ID",
                updateData: data
            })
        }
    })
}
// Xây dựng API getCommentById
const getCommentById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.commentId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(400).json({
            message: "Dữ liệu Id không đúng"
        })
    }
    //Thực hiện gọi
    commentModel.findById(Id).populate("postId").exec((err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "Tải toàn bộ dữ liệu thành công thông qua ID",
                Comment: data
            })
        }
    })
}
// Xây dựng API deleteCommentById
const deleteCommentById=(req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.commentId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(400).json({
            message: "Dữ liệu Id không đúng"
        })
    }
    //Thực hiện xóa
    commentModel.findByIdAndDelete(Id).populate("postId").exec((err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "Xóa dữ liệu thành công thông qua ID",
                Comment: data
            })
        }
    })
}
// Xây dựng API getCommentsOfPost
const getCommentsOfPost = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.postId;
    let condition = {} // lọc theo {key:value}
    if(Id){
        condition.postId =Id
    }
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(400).json({
            message: "Dữ liệu Id không đúng"
        })
    }
    commentModel.find(condition).populate("postId").exec((err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "lọc dữ liệu thành công thông qua ID",
                Comment: data
            })
        }
    })
}
//exprot modul
module.exports = {
    createComment,
    getAllComment,
    updateCommentById,
    getCommentById,
    deleteCommentById,
    getCommentsOfPost
}