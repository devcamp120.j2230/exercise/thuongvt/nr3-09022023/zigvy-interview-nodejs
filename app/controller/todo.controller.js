// import thư viện mongose 
const mongoose = require("mongoose");
//improt Model 
const todoModel = require("../model/todo.model");
//Xây dựng API createTodo:
const createTodo = (req,res)=>{
    //B1 thu thập dữ liệu từ req
    let body = req.body;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.userId)){
        return res.status(400).json({
            message: "userId không hợp lệ"
        })
    }
    if(!body.title){
        return res.status(400).json({
            message: 'dữ liệu title không đúng'
          })
    }
    if(typeof body.completed != "boolean"){
        return res.status(400).json({
            message: 'dữ liệu completed không đúng'
          })
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    // chuẩn bị dữ liệu
    let newTodo ={
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title,
        completed: body.completed
    }

    todoModel.create(newTodo,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"Tạo mới thành công",
                Todos: data
            })
        }
    })
}
// Xây dựng API getAllTodo
const getAllTodo = (req,res)=>{
    //B1 thu thập dữ liệu từ req
    let {userId} = req.query;
    let conditionCheck = {};
    //B2 Kiểm tra dữ liệu
    if(userId){
        conditionCheck.userId = userId
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    todoModel.find(conditionCheck)
        .populate("userId")
        .exec((err,data)=>{
            if(err){
                return res.status(500).json({
                    message: err.message
                })
            }
            else{
                return res.status(201).json({
                    message:"lấy thành công data",
                    Todos: data
                })
            }
    })
}
// Xây dựng API updateTodoById:
const updateTodoById =(req,res)=>{
    //B1 thu thập dữ liệu từ req
    let id = req.params.todoId;
    let body = req.body;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "TodoId không hợp lệ"
        })
    }
    if(!body.title){
        return res.status(400).json({
            message: 'dữ liệu title không đúng'
          })
    }
    if(typeof body.completed != "boolean"){
        return res.status(400).json({
            message: 'dữ liệu completed không đúng'
          })
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    let updateTodo ={
        userId: body.userId,
        title: body.title,
        completed: body.completed
    }
    todoModel.findByIdAndUpdate(id,updateTodo)
        .populate("userId")
        .exec((err,data)=>{
            if(err){
                return res.status(500).json({
                    message: err.message
                })
            }
            else{
                return res.status(201).json({
                    message:"update thành công",
                    Todos: data
                })
            }
    })
}
// Xây dựng API deleteTodoById:
const deleteTodoById =(req,res)=>{
    //B1 thu thập dữ liệu từ req
    let id = req.params.todoId;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "userId không hợp lệ"
        })
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    todoModel.findByIdAndDelete(id)
        .populate("userId")
        .exec((err,data)=>{
            if(err){
                return res.status(500).json({
                    message: err.message
                })
            }
            else{
                return res.status(201).json({
                    message:"xoá thành công",
                    Todos: data
                })
            }
    })
}
// Xây dựng API getTodoById:
const getTodoById =(req,res)=>{
    //B1 thu thập dữ liệu từ req
    let id = req.params.todoId;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    todoModel.findById(id)
        .populate("userId")
        .exec((err,data)=>{
            if(err){
                return res.status(500).json({
                    message: err.message
                })
            }
            else{
                return res.status(201).json({
                    message:"tải thành công thông qua id",
                    Todos: data
                })
            }
    })
}
// Xây dựng API getTodosOfUser:
const getTodosOfUser = (req,res)=>{
    //B1 thu thập dữ liệu từ req
    let userId = req.params.userId;
    let condition = {};
    //B2 Kiểm tra dữ liệu
    if(userId){
        condition.userId = userId
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    todoModel.find(condition)
    .populate("userId")
        .exec((err,data)=>{
            if(err){
                return res.status(500).json({
                    message: err.message
                })
            }
            else{
                return res.status(201).json({
                    message:"tải thành công dữ liệu",
                    todo: data
                })
            }
        })
}
//exprot controller
module.exports = {
   createTodo,
   getAllTodo,
   updateTodoById,
   deleteTodoById,
   getTodoById,
   getTodosOfUser
 }
