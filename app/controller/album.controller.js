// import thư viện mongose 
const mongoose = require("mongoose");
//improt Model 
const albumModel = require("../model/album.model");
//Xây dựng API createAlbum:
const createAlbum = (req, res) => {
    //B1 thu thập dữ liệu từ req
    let body = req.body;
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(body.userId)) {
        return res.status(400).json({
            message: "postId không hợp lệ"
        })
    }
    if (!body.title) {
        return res.status(400).json({
            message: 'dữ liệu title không đúng'
        })
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    // chuẩn bị dữ liệu
    let newAlbum = {
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title
    }

    albumModel.create(newAlbum, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "Tạo mới thành công",
                Album: data
            })
        }
    })
}
// Xây dựng API getAllAlbum
const getAllAlbum = (req, res) => {
    //B1 thu thập dữ liệu từ req
    let { userId } = req.query;
    let conditionCheck = {};
    //B2 Kiểm tra dữ liệu
    if (userId) {
        conditionCheck.userId = userId
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    albumModel.find(conditionCheck).populate({ path: "userId" }).exec((err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "Tải thành công toàn bộ album",
                Album: data
            })
        }
    })
}
// Xây dựng API updateAlbumById:
const updateAlbumById = (req, res) => {
    //B1 thu thập dữ liệu từ req
    let id = req.params.albumId;
    let body = req.body;
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    if (!body.title) {
        return res.status(400).json({
            message: 'dữ liệu title không đúng'
        })
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    let updateAlbum = {
        userId: body.userId,
        title: body.title
    }
    albumModel.findByIdAndUpdate(id, updateAlbum,(err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "cập nhật thành công dữ liệu",
                Album: data
            })
        }
    })
}
// Xây dựng API getAlbumById
const getAlbumById = (req, res) => {
    //B1 thu thập dữ liệu từ req
    let id = req.params.albumId;
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    albumModel.findById(id)
        .populate("userId")
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    message: err
                })
            }
            else {
                return res.status(201).json({
                    message: "tải thành công dữ liệu thông qua id",
                    Album: data
                })
            }
        })
}
// Xây dựng API deleteAlbumById:
const deleteAlbumById = (req, res) => {
    //B1 thu thập dữ liệu từ req
    let id = req.params.albumId;
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    albumModel.findByIdAndDelete(id, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "xoá thành công dữ liệu thông qua id",
                Album: data
            })
        }
    })
}
// Xây dựng API getAlbumsOfUser:
const getAlbumsOfUser = (req, res) => {
    //B1 thu thập dữ liệu từ req
    let Id = req.params.userId;
    let condition = {};
    //B2 Kiểm tra dữ liệu
    if (Id) {
        condition.userId = Id
    }
    //B3 thự gọi model thực hiện thao tác nghiệp vụ
    albumModel.find(condition)
        .populate("userId")
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    message: err.message
                })
            }
            else {
                return res.status(201).json({
                    message: "tải thành công dữ liệu",
                    Album: data
                })
            }
        })
}

//exprot controller
module.exports = {
    createAlbum,
    getAllAlbum,
    updateAlbumById,
    getAlbumById,
    deleteAlbumById,
    getAlbumsOfUser
}
