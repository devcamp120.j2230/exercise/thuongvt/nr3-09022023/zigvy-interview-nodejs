// Khai bá thư viện express
const express = require("express");
// Khai báo thư viện mongosse
const mongoose = require("mongoose");
// Khai báo thư viện dường dẫn
const path = require("path");
const { albumRouter } = require("./app/router/album.router");
const { commentRouter } = require("./app/router/comment.router");
const { photoRouter } = require("./app/router/photo.router");
const { postRouter } = require("./app/router/post.router");
const { todoRouter } = require("./app/router/todo.router");
//Khái báo các router
const { userRouter } = require("./app/router/users.router");


//Tạo app express
const app = express();
//khai báo cổng chạy 
const port = 8000
//sử dụng được body json
app.use(express.json());
//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}));
// Sử dụng mã cho mongodb từ v6 trở lên 
mongoose.set('strictQuery', true);
// kết nối mongoDB
mongoose.connect(`mongodb://localhost:27017/Zigvy_Interview`, function(error) {
    if (error) throw error;
    console.log('Kết nối thành công với mongoDB');
});


// sử dụng router
app.use(userRouter);
app.use(postRouter);
app.use(commentRouter);
app.use(albumRouter);
app.use(photoRouter);
app.use(todoRouter);

app.listen(port,()=>{
    console.log(`App đã chạy trên cổng : ${port}`)
})